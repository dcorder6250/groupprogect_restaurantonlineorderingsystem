/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package onlinerestaurantorderingsystem;

/**
 *
 * @author Daniel
 */
public class Cart 
{
    String description = " "; 
    double productPrice = 0.0;
    static double subtotal = 0.0;
    static int numberOfItemsInCart = 0;
    
    Cart()
    {
        
    }
    
    Cart(String name, double price, int numItems)
    {
        this.description = name;
        this.productPrice = price;
        subtotal += productPrice * numItems;
    }
    
    public void setDescription(String name)
    {
        description = name;
    }
    
    public void setProductPrice(double price)
    {
        productPrice = price;
    }
    
    public static void setNumberOfItemsInCart(int numItems)
    {
        numberOfItemsInCart += numItems;
    }
    
    public void setSubtotal(int numItems)
    {
        subtotal = productPrice * numItems;
    }
    
    
    public String getDescription()
    {
        return description;
    }
    
    public double getProductPrice()
    {
        return productPrice;
    }
    
    public static double getSubtotal()
    {
        return subtotal;
    }
    
    public static int getNumberOfItemsInCart()
    {
        return numberOfItemsInCart;
    }

}
