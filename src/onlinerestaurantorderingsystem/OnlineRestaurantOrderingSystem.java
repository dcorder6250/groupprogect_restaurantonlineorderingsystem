/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package onlinerestaurantorderingsystem;

import java.util.ArrayList;
import java.util.Scanner;

/**
 * CIST 2372 Section 21305
 * Date: 
 * @author Daniel Cordero SID: 6250
 * Description: 
 */
public class OnlineRestaurantOrderingSystem 
{
    public static void main(String[] args) 
    {
        Pizza inventory[] = new Pizza[5];
        
        // Array indexes and their values 
        inventory[0] = new Pizza("Supreme", 11.0,"//Description goes here");
        inventory[1] = new Pizza("Meat Lovers", 12.0,"//Description goes here");
        inventory[2] = new Pizza("Veggie Lovers",13.0,"//Description goes here");
        inventory[3] = new Pizza("Pepperoni Lovers", 14.0,"//Description goes here");
        inventory[4] = new Pizza("Ultimate Cheese Lovers",15.0,"//Description goes here");
        
       
        
        // Creates a new ArrayList of the objects of Shopping_Cart with
        // the variable cart.
        ArrayList<Cart> cart = new ArrayList<>();
        
        // Invokes the displayWelcomeMessage() method
        displayWelcomeMessage();
        
        // Invokes the displayMenu() method with the arguement of cart
        // and is set to an integer choice variable.
        int choice = displayMenu(cart);
        
        // start of do-while loop
        do
        {            
            if (choice == 1)    // if choice is equal to 1  
            {   
                // Invokes showProductMenu() method with inventory and cart
                // as its arguements.
                showProductMenu(inventory, cart);
            }
            else if (choice == 3)   // if choice is equal to 4
            {
                // checkout() mehthod is invoked with cart as arguement
                checkout(cart);
            }
          // do-while condition, while choice is not equal to 5
        } while(choice != 4);
        
    }
    
    // displayWelcomeMessage() method
    public static void displayWelcomeMessage()
    {
        // Displays a welcome message to user
        System.out.println("\nWelcome! Thank you for visiting "
                + "GTC Pizza Online!");
    }
    
    // displayMenu() method with ArrayList type cart as parameter
    public static int displayMenu(ArrayList<Cart> cart)
    {
        // creates a new Scanner object
        java.util.Scanner input = new java.util.Scanner(System.in);        
        
        // Displays menu items for Main Menu
        System.out.println("\n\nMain Menu");
        System.out.println("-------------------");
        System.out.println("(1) View Pizzas");           
        System.out.println("(2) Checkout");
        System.out.println("(3) Exit");
        
        // Prompts user to enter their selection from menu
        System.out.print("\nPlease make a selection from "  
                + "the options above: ");
        
        // Input from user is set to selection variable.
        int selection = input.nextInt();
        
        // if selection is equal to 4
        if (selection == 2)
            {
                // checkout() method is invoked with cart as arguement
                checkout(cart);
            }
        // else if selection is equal to 5 
        else if (selection == 3)
            {
                // exitProgram() menthod is invoke and exits the program
                exitProgram();
            }
        
        // return selection to calling method
        return selection;       
    }
    
    // showProductMenu() method with Product_Inventory type inventory and 
    // ArrayList type cart as its parameters
    public static void showProductMenu(Pizza pizza[],
            ArrayList<Cart> cart)
    {
        // Display menu items with Product Description, Product Catagory, and 
        // Product Price
        System.out.println("\nPizza     Product Description     "
                + "Product Price");
        System.out.println("-----------------------------------------------"
                + "-----------");
        System.out.println("(1)  Supreme                   //Description goes here//     "
                + "$11.00");
        System.out.println("(2)  Meat Lovers               //Description goes here//     "
                + "$12.00");    
        System.out.println("(3)  Veggie Lovers             //Description goes here//     "
                + "$13.00");
        System.out.println("(4)  Pepperoni Lovers          //Description goes here//     "
                + "$14.00");
        System.out.println("(5)  Ultimate Cheese Lovers    //Description goes here//     "
                + "$15.00");
               
        
        // Prompts the user to select an item to purchase by entering 1-16
        System.out.print("\nPlease select an item you wish to add to Shopping Cart <1-5>: ");
        java.util.Scanner input = new java.util.Scanner(System.in);
        int selection = input.nextInt();
        
        // Prompts the user to enter the quantity of the item they want to purchase
        System.out.print("\nHow many would you like to purchase: ");
        int numItems = input.nextInt();
        
        // Prompts the user to enter a new amount
//        System.out.println("Please enter a new amount.");
//        numItems = input.nextInt();
        
    
            // addToCart() method is invoked with parameters and set to cart
            cart = addToCart(pizza, cart, selection, numItems);
        
            // Invokes showCart() method 
            showCart(cart);
        
            // Invokes displayMenu() method
            displayMenu(cart);
        
    }
    
    // addToCart() method with its parameters and returning an ArrayList type
    public static ArrayList<Cart> addToCart(Pizza pizza[],
            ArrayList<Cart> cart, int selection, int numItems)
    {
        // Invokes add() method from ArrayList class, adding the product name,
        // product price, and number of items to the ArrayList of Shopping_Cart objects
        cart.add(new Cart(pizza[selection - 1].name, 
                pizza[selection - 1].price, numItems));
        
        // Invokes setNumberOfItemsInCart() method from Shopping_Cart class 
        // with numItems as parameter
        Cart.setNumberOfItemsInCart(numItems);
        
        // return ArrayList type cart
        return cart;
            
    }
    
    // showCart() method with its parameters and returning an ArrayList type 
    public static ArrayList<Cart> showCart(ArrayList<Cart> cart)
    {
        // Prints out Shopping Cart heading
        System.out.println("\nShopping Cart");
        System.out.println("-------------");
        
        // for loop
        for (int i = 0; i < cart.size(); i++)
        {
            // Display format to output description and price referenced by
            // cart object variable.
            System.out.printf("%d %s $%4.2f",i + 1,cart.get(i).getDescription(),
                    cart.get(i).getProductPrice());
            System.out.println(" ");
            
        }
        
        // Display format to outputting the subtotal from Shopping_Cart class
        System.out.printf("\tSubtotal:   $%4.2f", Cart.getSubtotal());
        
        // Displays the number of items in cart 
        System.out.println("\nNumber of items in Shopping Cart: " +
                Cart.getNumberOfItemsInCart());
        
        // returns cart
        return cart;
    }
    
    // checkout() method with cart as its parameter
    public static void checkout(ArrayList<Cart> cart)
    {
        // Invokes calculateOrderTotal with cart as parameter
        // and is set to total.
        double total = calculateOrderTotal(cart);
        
        // Invokes reciecePayment() method with total parameter
        recievePayment(total);
        
        //Invokes printOrderConfirmations() method 
        printOrderConfirmations();
        
        // Invokes exitProgram() method
        exitProgram();
    }
    
    // recievePayment() method with parameter total
    public static void recievePayment(double total)
    {
        double payment;   // Declares double variable payment
        
        // Creates new Scanner object
        java.util.Scanner input = new java.util.Scanner(System.in);
        
        // Prompts the user to enter amount to pay
        System.out.print("\nEnter amount to be paid: ");
        payment = input.nextDouble();
        
        // if total is greater than payment
        if (total > payment)
        {
            // calculation to obtain amount owed
            double stillOwe = total - payment;
            
            // Displays remaining amount to be paid
            System.out.println("\n$" + stillOwe + " is still remaining.");
            System.out.println("\nPlease pay the remaining amount.");
            
            // Prompts user to enter remaining amound
            System.out.print("\nEnter amount to be paid: ");
            payment = input.nextDouble();                    
        }
        
        CreditCardNumber();
        
        // Calculation to obtain totalPayment
        double totalPayment = ((total - payment) * 100) / 100.0;
        
        // Displays change amount owed to user
        System.out.println("\nChange: $" + totalPayment);        
    }
    
    public static void CreditCardNumber()
    {
                int amexTotal = 0;                  // holds total Amex cards processed
        int visaTotal = 0;                  // holds total Visa cards processed
        int masterCardTotal = 0;            // holds total Master Cards processed
        int discoverTotal = 0;              // holds total Discover cards processed
        long total = 0;                     // to hold sum of odd and even digits
        int again;                          // Sentinel value 
        int totalValidCreditCards = 0;      // Total number of valid cc#'s processed
        int totalInvalidCreditCards = 0;    // Total number of invalid cc#'s processed
        
        // do-while loop that validates the credit card number entered
        // and asks the user if they wish to enter another card.
        do
        {           
            // Invokes getCreditCarNumber method and sets
            // return value to creditCardNumber.
            long creditCardNumber = getCreditCardNumber();
            
            // creditCardNumber variable set to number variable to hold
            // value through current iteration.
            long number = creditCardNumber;
            
            // Gets the number of digits of the card number entered.
            int length = String.valueOf(creditCardNumber).length();
            
            // if statement to validate the number of digits in credit card.
            if (length <=16 && length >= 13)
            {
                long totalOddDigitNumber = 0;   // to hold accumulating odd digit values
                long totalEvenDigitNumber = 0;  // to hold accumulating even digits values
                
                // while loop to determine if credit card number is
                // valid taken down to last remaining digit.
                while (creditCardNumber != 0) 
                {
                    // getOddDigit method with creditCardNumber as arguement
                    // set to accumulate totalOddDigitNumber variable.
                    totalOddDigitNumber += getOddDigit(creditCardNumber);        
            
//                    System.out.println("Total Odd Digits: " + totalOddDigitNumber);
            
                    // Invokes getRemainingNumber method with creditCardNumber as arguement
                    // set to remainingNum variable
                    long remainingNum = getRemainingNumber(creditCardNumber);
                    
                    // Invokes getEvenDigit method with remainingNum as arguement
                    // set to accumulate totalEvenDigitNumber variable. 
                    totalEvenDigitNumber += getEvenDigit(remainingNum);  
            
//                    System.out.println("Total Even Digits: " + totalEvenDigitNumber);
                
                    // if else statements to determine type of credit card
                    // and accumulates the type of card.
                    if (remainingNum == 37 )
                    {
                        // Accumulates American Express 
                        amexTotal++;
                    }
                    else if (remainingNum == 4)
                    {
                        // Accumulates Visa 
                        visaTotal++;
                    }
                    else if (remainingNum == 5)
                    {
                        // // Accumulates Master Card
                        masterCardTotal++;
                    }
                    else if (remainingNum == 6)
                    {
                        // // Accumulates Discover
                        discoverTotal++;
                    }                   
            
                    // Invokes getRemainingNumber method is invoked with 
                    // remainingNum as arguement and set to creditCardNumber
                    creditCardNumber = getRemainingNumber(remainingNum);
                
                    // sum of totalEvenDigitNumer and totalOddDigitNumber
                    // and set to total variable.
                    total = totalEvenDigitNumber + totalOddDigitNumber;
                    
//                    System.out.println("Sum of digits: " + total);            
                }
                
                // if total is not divisible by 10
                // then the credit card number entered is not valid,
                // otherwise the credit card will be displayed as valid.
                if (total % 10 != 0)
                {
                    // Displays credit card number is invalid.
                    System.out.println("\n" + number + " is INVALID.\n");
                    
                    // Increments total number of invalid credit cards prccessed.
                    totalInvalidCreditCards++;
                }                
                else
                {
                    // Displays credit card number is valid
                    System.out.print("\n" + number + " is VALID.\n");
                    
                    // Increments total number of valid credit cards processed.
                    totalValidCreditCards++;
                }               
            } 
            
            // else statement to display the card is invalid in reference 
            // the length of the number of digits of the credit card
            // number entered.
            else
            {
                // Display credit card number is invalid.
                System.out.println("\n" + number + " is INVALID.\n");
                
                // Increments total number of invalid credit cards.
                totalInvalidCreditCards++;
            }
            
            // Ivokes getAnswer method with no arguements
            // and set to again variable.
            again = getAnswer();           
               
        } while (again != 0);   // Condition of do while loop, if again is not 
                                // equal to 0.
        
        // Invokes getTotals method with amexTotal,visaTotal,
        // masterCardTotal,discoverTotal variables as arguements.
        getTotals(amexTotal,visaTotal,masterCardTotal,discoverTotal);
        
        // If statement with the condition containing again variable
        // to determine if user chose to end loop and displays totals.
        if (again == 0)
        {
            // Displays total number of invalid credit cards.
            System.out.println("\nTotal Number of Invalid Credit Cards "
                    + "Processed: " + totalInvalidCreditCards);
                        
            // // Displays total number of valid credit cards.
            System.out.println("\nTotal Number of Valid Credit Cards "
                    + "Processed: " + totalValidCreditCards);
            System.out.println("-------------------------------------------------");
        }
    }
    
    // getCreditCardNumber method
    // Prompts the user to enter a credit card number
    // and returns a long integer.
    public static long getCreditCardNumber()
    {
        // Creates a new Scanner object.
        Scanner input = new Scanner(System.in);       
        
        // Displays a prompt for the user to enter a credit card number.
        System.out.print("\nEnter a credit card number: ");
        long creditCardNumber = input.nextLong();
        
        // value of creditCardNumber variable is returned to calling method. 
        return creditCardNumber;
    }
    
    // getRemainingNumber method
    // Gets the remaining number of credit card after the last digit 
    // has been removed
    public static long getRemainingNumber(long n)
    {
        // String.valueOf method is invoked with the arguement value of
        // the n variable and set to the length variable.
        long length = String.valueOf(n).length();
        
        // If statement the determines the value of n and the number
        // of digits it contains. 
        // if n is not equal to zero and length is not greater than 1
        if (n != 0 && length > 1)
        {
            // n variabe is divided by to get and set to remainingNumer
            // variable to obtain the remain value of n.
            long remainingNumber = n / 10;   // remaining # - 23
//            System.out.println("Last remaining number: " + remainingNumber);
        
            // The value of remainingNumber is returned to calling method.
            return remainingNumber;
        }
        // If n is equal to zero and the length of n is 1
        // zero is returned to the calling method.
        else
            return 0;
    }
    
    // getOddDigit method
    // Gets the odd digit from credit card number with
    // n as parameter.
    public static long getOddDigit(long n)
    {      
        // n variable is modded by 10 to obtain odd digit
        // and is set to oddDigitNumber variable.
        long oddDigitNumber = n % 10;    
        
//        System.out.println("Odd Digit number: " + oddDigitNumber);      
        
        // Value of oddDigitNumber variable is returned to calling method.
        return oddDigitNumber;
    }
    
    // getEvendigit method
    // Gets the even digit from credit card number with
    // n as parameter. 
    public static long getEvenDigit(long n)
    {
        // n variable is modded by 10 to obtain even digit
        // and is set to evenDigitNumber variable.
        long evenDigitNumber = n % 10;
        
//            System.out.println("Even Digit Number: " + evenDigitNumber);
        
        // Value of evenDigitNumber variable is multiplied by 2
        // and set to newEvenDigitNumber.
        long newEvenDigitNumber = evenDigitNumber * 2;
        
        // If statement to determine if newEvenDigitNumber is greater
        // than 9.        
        if (newEvenDigitNumber > 9)
        {
            // newEvenDigitNumber variable is modded by 10 to remove last digit
            // and is set to lastEvenDigit variable.
            long lastEvenDigit = newEvenDigitNumber % 10;
                
//          System.out.println("Last even digit: " + lastEvenDigit);                
                
            // Value of newEvenDigitNumber is divided by 10 and set to 
            // remainingEven 
            long remainingEven = newEvenDigitNumber / 10;                
                
//          System.out.println("Remaining even number: " + remainingEven);
                
            // Values of lastEvenDigit and remainingEven variables are 
            // add and set to summedEvenNumber.
            long summedEvenNumber = lastEvenDigit + remainingEven;
                
            // summedEvenNumber is set to newEvenDigitNumber.
            newEvenDigitNumber = summedEvenNumber;
        }
        
        // Returns the value of newEvenDigitNumber to calling method.
        return newEvenDigitNumber;
    }
    
    // getTotals method with parameters of a, v, mc, and d.
    // Displays accumulated totals of types of cards processed
    public static void getTotals(int a, int v, int mc, int d)
    {
        // Displays totals.
        System.out.println("\n---------------------------------------------------");
        System.out.println("American Express: " + a);
        System.out.println("Visa: " + v);
        System.out.println("Master Card: " + mc);
        System.out.println("Discover: " + d);        
    }
    
    // getAnswer method
    // Prompts the user to enter a response if they wish to process another
    // credit card along with input validation.
    public static int getAnswer()
    {
        // Creates new Scanner object
        Scanner input = new Scanner(System.in);
        
        // Displays a prompt asking the user to enter another
        // credit card numner.
        System.out.println("\nWould you like to process another "
                + "credit card number?");
                
        // Displays choice for user reponse.
        System.out.print("\nEnter <1> for YES or <0> for <NO>: ");
        int answer = input.nextInt();
        
        // Input validation
        // If the user enters an invalid input, an error message is displayed
        // and askes the user to enter a correct input.
        if (answer != 1 && answer != 0)
            {
                // Display error message with the the incorrect input.
                System.out.println("\nERROR!!! Invalid input entered! " + answer 
                        + " is not a correct response.");   
                
                // getAnswer method is invoked again and set to 
                // answer variable 
                answer = getAnswer();
            }   
        // returns value of the variable answer.
        return answer;
    }            
    
    // calculateOrderTotal() method with parameter
    public static double calculateOrderTotal(ArrayList<Cart> cart)
    {
        // Displays Checkout heading
        System.out.println("\nCheckout");
        System.out.println("----------");
        
        // for loop
        for (int i = 0; i < cart.size(); i++)
        {
            // Display format outputting description and product price
            // reference by cart object variable
            System.out.printf("%d %s $%4.2f",i + 1,cart.get(i).getDescription(),
                    cart.get(i).getProductPrice());
            System.out.println(" ");
            
        }
        
        // Display subtotal form Shopping_Cart class
        System.out.printf("\n\tSubtotal:   $%.2f", Cart.getSubtotal());
        
        // Displays number of items in shopping cart from Shopping_Cart class
        System.out.println("\n\nNumber of items in Shopping Cart: " +
                Cart.getNumberOfItemsInCart());
        
        // Prompts user to enter tax rate
        System.out.print("\nEnter tax rate: ");
        java.util.Scanner input = new java.util.Scanner(System.in);
        double taxRate = input.nextDouble();
        
        
        double orderTotal = 0;  // Declares orderTotal and sets it to 0
        
        // getSubtotal() method is invoked within Shopping_Cart class
        // and is set to suntotal variable.
        double subtotal = Cart.getSubtotal();
        
        // getNumberOfItemsInCart() method is invoked and is set to numberOfItems
        int numberOfItems = Cart.getNumberOfItemsInCart();
        
        // for loop
        for (int i = 0; i < cart.size(); i++)
        {
            // Calculation to obtain orderTotal
            orderTotal = subtotal + (subtotal * taxRate) * numberOfItems;
        }
        
        // Calculation to obtain taxAmount
        double taxAmount = ((subtotal * taxRate) * 100) / 100.0;
        
        // Displays tax amount
        System.out.printf("\nTax Amount:  $%.2f", taxAmount);
        
        // Displays order total
        System.out.println("\n\nOrder Total: $" + orderTotal);       
        
        // returns orderTotal
        return orderTotal;
    }
    
    // printOrderConfirmation() method
    public static void printOrderConfirmations()
    {
        // Creats new Order object 
        OrderConfirmation order = new OrderConfirmation();
        
        // Prints out order confirmation number 
        System.out.println("\nOrder Confirmation Number: " + order.confirmationsNumber);
    }
    
    // exitProgrma() method
    public static void exitProgram()
    {
        // Display thank you message to user and exits the program
        System.out.println("\nThank you for shopping with us today!");
        System.exit(0);
    }

}

// Test credit card numbers
// ---------------------------------
// Amex     371449635398431
// Master   5555555555554444
// Discover 6011000990139424
// Visa     4388576018402626 invalid
// Visa     4388576018410707 valid
